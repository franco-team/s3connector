var express = require('express');
var app = express();
var bodyParser = require("body-parser");

const readline = require('readline');

// Load the SDK for JavaScript
var AWS = require('aws-sdk');
// Set the region

AWS.config.loadFromPath('config.json');

//permite actualizar por properties
//AWS.config.update({region: 'sa-east-1'});

// Create S3 service object
s3 = new AWS.S3({apiVersion: '2006-03-01'});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


// Call S3 to list current buckets
app.get('/buckets', function (req, res) {

  s3.listBuckets(function(err, data) {
     if (err) {
        res.send(err)
      } else {
        res.send(data.Buckets);
     }
  });
});

app.get('/buckets/:bucket/items/:item', function(req, res) {
  let bucketName = req.params.bucket;
  let itemName = req.params.item;

  var params = {
    Bucket: bucketName,
    Key: itemName
   };

   s3.getObject(params, function(err, data){
      if (err) {
        res.send(err);
      } else {
        res.send( Buffer.from(data.Body).toString('utf8'));
      }
   });

   app.post('/test', function (req, res) {
       res.send('funciono POST');
   });

   app.post('/buckets/:bucket/items/', function (req, res) {
       let bucketName = req.params.bucket;
       let fileName = req.body.fileName;
       let fileContent = req.body.fileContent;
       let fileContentType = req.body.contentType;
       const data = {
           Bucket: bucketName,
           Key: fileName,
           ContentEncoding : 'base64',
           ContentType : fileContentType,
           Body: Buffer.from(fileContent, 'base64')
       };

       s3.putObject(data, function(err, data){
           if (err) {
               console.log('Error uploading data: ', data);
               res.send(err);
           } else {
               res.send('succesfully uploaded file');
           }
       });
        res.send('adsfasd')
   });

  //res.send(s3bucket.getObject({Key: 'files/datos.txt'}));
  //res.send('test')
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
